#!/bin/bash

# This script is designed to be idempotent; it can be run
# numerous times without ill effect.

# Prerequisites:
#   Add the bitbucket_laptop SSH key        (chmod 600)
#   Install git
#   Pull ~/scripts dir                      (chmod u+x)

# Then this script should be ready to run
cd "$HOME" || exit
has_zenity=$(which zenity)

if [[ ! -z "$has_zenity" && "$has_zenity" ]]
then
	zenity --question --title="Install" --text="Begin new installation setup?" --timeout=6

	# For some strange reason.......
	# The Zenity --question option will exit with one of {0,1,5} depending on user selection but will always return 0
  # Therefore, it is impossible to assign the return value to a variable and instead
	# there is no choice but to check the exit code of the last issued command using $?
  #
	# 0 for Yes, 1 for No, 5 for timeout
	if (( $? == 0 ))
	then
		printf "Beginning new PC setup.....\n\n";
	else
		printf "You have aborted the installation.\nTerminating......\n\n";
		exit;
	fi
fi

printf "\n\n\n"
if ! [ -f "$HOME/.vimrc" ]; then
  if [ -f "$HOME/scripts/.vimrc" ]; then
    ln -s "$HOME/scripts/.vimrc" "$HOME/.vimrc"
  else
    printf "set number\nset tabstop=4 softtabstop=0 expandtab shiftwidth=2 smarttab\n" > "$HOME/.vimrc";
  fi
fi

if ! [ -f "$HOME/.nanorc" ]; then
  if [ -f "$HOME/scripts/.nanorc" ]; then
    ln -s "$HOME/scripts/.nanorc" "$HOME/.nanorc"
  else
    printf "set tabsize 2\nset tabstospaces\n" > "$HOME/.nanorc";
  fi
fi

if [ -f "$HOME/.bashrc" ] && [ ! -L "$HOME/.bashrc" ] && [ -d "$HOME/scripts" ]; then
  mv "$HOME/.bashrc" "$HOME/.bashrc.bak";
  ln -s "$HOME/scripts/.bashrc" "$HOME/.bashrc";
  source "$HOME/.bashrc";
fi

# Bash source file highlighing
has_srch=`type source-highlight 2>/dev/null`

if [[ -z "$has_srch" ]]
then
  has_zenity=$(which zenity)

  if [[ ! -z "$has_zenity" && "$has_zenity" ]]
  then
          zenity --question --title="Install" --text="The source-highlight package is missing.\nWould you like to install it?" --width=350 --timeout=6

          # For some strange reason.......
          # The Zenity --question option will exit with one of {0,1,5} depending on user selection but will always return 0
    # Therefore, it is impossible to assign the return value to a variable and instead
          # there is no choice but to check the exit code of the last issued command using $?
    #
          # 0 for Yes, 1 for No, 5 for timeout
          if (( $? == 0 ))
          then
                  printf "Installing source-highlight package........\n\n";
                  sudo apt-get install source-highlight
          fi
  else
    read -p "The source-highlight package is missing. Would you like to install it: " install_srch

    if [[ "$install_srch" =~ ^[yY] ]]
    then
      printf "\n"
      sudo apt-get install source-highlight

      if [ ! "$uuid" -eq 0 ]; then
        printf "\n\n\nEnter sudo password to personalize the highlighting colors:\n\n";
      fi

      # Use my personal styling file
      if [ ! -f /usr/share/source-highlight/joe.style ] && [ -f joe.style ]; then
        sudo ln -sr joe.style /usr/share/source-highlight/joe.style
        printf "\n\n\n.....Created symlink to joe.style in Source Highlight directory (/usr/share/source-highlight).\n\n";
      fi

    fi
  fi
fi

# Remove Linux yarn since this conflicts with yarn in JavaScript/Node
has_yarn=`type yarn`
if  [ "$has_yarn" ]; then
  printf "Removing yarn package (cmdtest).\nThis is to avoid conflicts with JavaScript/Npm yarn.\n\n";
  sudo apt remove --purge cmdtest
fi

if ! [ -d "$HOME/Documents" ]; then
  rm -rf "$HOME/Documents";
  mkdir "$HOME/Documents";
  git clone git@bitbucket.org:/willrun4cake/documents.git Documents;
fi

git_has_config_email=$(git config -l | grep user.email)
git_has_config_username=$(git config -l | grep user.name)

if [[ -z "$git_has_config_email" ]] && [[ -z "$git_has_config_username" ]]; then
  git config --global user.name  "willrun4cake"
  git config --global user.email "crazyforcabinets@gmail.com"
fi

if [ -L "$HOME/.ssh" ]; then
  printf "Symlink for .ssh already exists: Skipping .ssh setup\n\n";
elif [ -f "$HOME/.ssh" ]; then
  printf "%s/.ssh is a regular file. Check if %s/.ssh should be a symlink.\n\n" "$HOME" "$HOME";
  exit
elif ! [ -d "$HOME/.ssh" ]; then
  printf "No .ssh directory was detected. Terminating setup.....\n\n";
elif [ -d "$HOME/.ssh" ]; then
  # .ssh is a directory. We want .ssh to be a symlink to the versioned directory.

  # Setup SSH
  cd "$HOME/.ssh" || (printf "No .ssh directory found. Terminating setup....\n\n" && exit)
  bb_found=$(find . -iname 'bitbucket_laptop');
  if [[ -z $bb_found ]]; then
    printf "No Bit Bucket credentials were found in %s/.ssh.\nPlease add the Bit Bucket credentials then retry.\nTerminating setup.....\n\n" $HOME;
    exit;
  fi

  if [ -d "$HOME/Documents/SSH/Desktop" ]; then
    cd "$HOME" || (printf "No .ssh directory found. Terminating setup.....\n\n" && exit)
    mv "$HOME/.ssh" "$HOME/.ssh.bak";
    ln -s "$HOME/Documents/SSH/Desktop" "$HOME/.ssh";
  else

    if [ ! -f "$HOME/.ssh/bitbucket_passphrase.sh" ]; then
      if [ -d "$HOME/.ssh" ]; then
        printf "#\!\/bin\/sh\necho 16\&Under" > "$HOME/.ssh/bitbucket_passphrase.sh";
        chmod u+x "$HOME/.ssh/bitbucket_passphrase.sh";
        chmod g+x "$HOME/.ssh/bitbucket_passphrase.sh";
      else
        printf "No $HOME/.ssh directory exists. Terminating setup....\n\n";
      fi
    fi

    chmod 600 "$HOME/.ssh/bitbucket_laptop";

    DISPLAY=1 SSH_ASKPASS="$HOME/.ssh/bitbucket_passphrase.sh" ssh-add "$HOME/.ssh/bitbucket_laptop" < /dev/null;

    bb_key_added=$(ssh-add -l | grep 'bitbucket');
    if [ -z $bb_key_added ]; then
      printf "Unable to add SSH key for Bit Bucket to ssh agent. Check .ssh directory.\n\n";
    fi

    cd "$HOME" || exit
    if ! [ -d "$HOME/Documents" ]; then
	    mkdir Documents;
	    git clone git@bitbucket.org:/willrun4cake/documents.git Documents;
	    if [ -d "$HOME/.ssh" ]; then
	      mv "$HOME/.ssh" "$HOME/.ssh.bak";
	    fi
	    ln -s "$HOME/Documents/SSH/Desktop" .ssh;
	fi
  fi
fi

# Add Bit Bucket key with passphrase
DISPLAY=1 SSH_ASKPASS="$HOME/.ssh/bitbucket_passphrase.sh" ssh-add ~/.ssh/bitbucket_laptop < /dev/null
printf "\n"

bb_key_added=$(ssh-add -l | grep 'bitbucket')
if [ -z "$bb_key_added" ]; then
  printf "Unable to add SSH key for Bit Bucket to ssh agent. Terminating setup....\n\n";
  exit;
fi

cd "$HOME" || exit

# Check /var/www/html, if DNE then add it for mounting sshfs dirs
if ! [ -d /var/www/html ]; then
  printf "Creating /var/www/html directory. Sudo permissions may be requested.\n\n";
  sudo mkdir -p /var/www/html
  cd /var/www || (printf "This setup requires executable permissions on /var/www\n\nTerminating....\n\n" && exit)
  printf "Setting ownership for /var/www directory.\nSudo permissions may be requested.\n\n";
  sudo chown -R root:"$USER" html
  sudo chmod -R 775 html

  # Check if user_allow_other is set in /etc/fuse.conf
  allow_blocked=$(less /etc/fuse.conf | grep -F '#user_allow_other')
  if ! [[ -z "$allow_blocked" || "$allow_blocked" == '' ]]; then
    # Replace/remove comment
    printf "Un-commenting #user_allow_other from /etc/fuse.conf.\nThis may require sudo permissions.\n\n";
    # sudo awk ???
  fi
fi

cd "$HOME" || exit

if ! [ -d  alwaysontop ]; then
  mkdir alwaysontop;
  git clone https://github.com/swirepe/alwaysontop.git alwaysontop;
fi
if ! [ -d ATI-Tool ]; then
  mkdir ATI-Tool;
  git clone git@bitbucket.org:willrun4cake/atitool.git ATI-Tool;
  no_ati=$(type atitool | grep -i 'not found')
  if [[ -z $no_ati || $no_ati == '' ]]; then
    has_atitool=$(echo $PATH | grep -i 'atitool');
    if [[ -z $has_atitool || $has_atitool == '' ]]; then
      export PATH=$PATH:$HOME/ATI-Tool
    fi
  fi
fi

if ! [ -d heroku ]; then
  mkdir -p heroku/blogapp;
  cd heroku || exit
# heroku install needs fixed. it is unique
  git clone https://git.heroku.com/evening-gorge-73831.git blogapp;
  cd "$HOME" || exit
fi

if ! [ -d Logins ]; then
  mkdir Logins;
  git clone git@bitbucket.org:/willrun4cake/logins.git.git Logins;
fi

if ! [ -d NodeProjects ]; then
  mkdir NodeProjects;
  git clone git@bitbucket.org:willrun4cake/nodeprojects.git NodeProjects;
fi

if ! [ -d Recipes ]; then
  mkdir Recipes;
  git clone git@bitbucket.org:willrun4cake/recipes Recipes;
fi

if ! [ -d Resumes ]; then
  mkdir Resumes;
  git clone git@bitbucket.org:willrun4cake/resumes.git Resumes;
fi

if ! [ -d Swingtech ]; then
  mkdir Swingtech;
  git clone git@bitbucket.org:willrun4cake/swingtech.git Swingtech;
fi

if ! [ -d Wallpapers ]; then
  mkdir Wallpapers;
  git clone git@bitbucket.org:willrun4cake/wallpapers.git Wallpapers;
fi

if ! [ -d /var/www/fastclosures ]; then
  printf "Creating /var/www/fastclosures. Sudo permissions may be requested.\n\n";
  sudo mkdir -p /var/www/fastclosures
  cd /var/www || exit
  sudo chown -R joe:joe fastclosures
  sudo chmod -R 774 fastclosures
  git clone git@bitbucket.org:willrun4cake/fastclosures.git fastclosures
  sudo mv /var/www/html/fastclosures /var/www/html/fasclosures.bak
  cd /var/www/html || exit
  ln -s /var/www/fastclosures/www fastclosures
  cd "$HOME" || exit
fi

# Install wine
has_wine=$(which wine)
install_wine=no
if [[ -z "$has_wine" || "$has_wine" == '' ]]; then
  if [[ ! -z "$has_zenity" && "$has_zenity" ]]
  then
	  zenity --question --title="Wine" --text="Would you like to install Wine?" --timeout=6

    if (( $? == 0 ))
    then
      install_wine=yes
    else
      printf "Skipping Wine installation.\n\n";
    fi
  else
    install_wine=yes
  fi

  if [[ "$install_wine" == 'yes' ]]
  then
  	printf "\n\nInstalling Wine.......\n\n";
  	sudo apt install -y wine
  	printf "\n\nInstalling Winetricks.....\n\n";
  	sudo apt install -y winetricks

    printf "\n\n\n***************** To configure Wine for 3DXChat ************* \n\n\n";
    printf "    1. Execute \"winecfg\" to open the configurator.\n    2. Set application to \"Windows 7\"\n";
    printf "    3. Add \"d3d11  (native,builtin)\" to Libraries > New override for library\n    4. Drives > Drive configuration\n\n        C:  ->   ../drive_c\n\n";
#   It lookse like these cannot be configured via the terminal
#   So provide the instructions above
  fi
else
  printf "Wine is already installed.\n";
fi

# Install 3DXChat
has_3dxchat=$(find "$HOME" -type f -iname "3DXChat.exe")
if [[ -n "$has_3dxchat" && "$has_3dxchat" != '' && "$has_3dxchat" ]]; then
  printf "3DXChat is already installed.\n";
else
  if [[ ! -z "$has_zenity" && "$has_zenity" ]]
  then
    zenity --question --title="Install 3DXChat?" --text="Would you like to install 3DXChat?" --timeout=6
    if (( $? == 1 ))
    then
      # An exit code of 1 indicates they selected "No"
      # So proceed as if 3DXChat is already installed
      has_3dxchat='yes'
    elif (( $? == 5 ))
    then
      has_3dxchat='yes'
      printf "\nSkipping 3DXChat installation because of timeout.\n\n";
    fi
  fi

  if [[ -z "$has_3dxchat" || "$has_3dxchat" == '' ]]; then
      printf "\n\nDownloading 3DXChat installer.....\n\n";
    rm -rf "$HOME/Downloads/3DXChat_installer.exe" > /dev/null
      wget -O "$HOME/Downloads/3DXChat_installer.exe" http://3dxchat.com/download.php
    printf "\n\nBeginning 3DXChat Installer.\n\n";
    wine "$HOME/Downloads/3DXChat_installer.exe"

  #  WINEPREFIX="$prefix/3DXChat Winetricks"
  #  PATH="$PATH:/path/to/winetricks"
  #	wine "$HOME/Downloads/3DXChat_installer.exe"

      has_xclip=$(which xclip)
      if [[ "$has_xclip" && -n "$has_xclip" ]]; then
          printf "aZQMJB3zHs" | xclip -selection clipboard -i 
          printf "\n\n3DXChat login passphrase has been copied to the clipboard.\n\n";
      printf "\n\n\nGame login info:\n    gelbelly@outlook.com\n    aZQMJB3zHs\n\n";
      fi
  else
      printf "3DXChat is already installed.\n";
  fi
fi

# Install Sublime Text, setup Themes
subl_installed=$(which subl)
snap_found=$(which snap)
add_themes=no
if [[ -z $subl_installed || "$subl_installed" == "" ]]; then
  if [[ -z $snap_found || "$snap_found" == "" ]]; then
  #  printf "\n\nUnable to install Sublime Text.\nYou need to install and start snap to install Sublime Text";
    printf "\n\nSnap is not installed. Attempting to add the Sublime Text repo.";
    wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
    echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
    sudo apt update && sudo apt install sublime-text
    add_themes=yes
  else
    if [[ "$snap_found" ]]; then
      snap_info=$(snap info "sublime-text")
      found_sublime=$(echo $snap_info | grep -o 'sublime')
      if [[ -z $found_sublime || "$found_sublime" == "" ]]; then
        printf "\n\nUnable to install Sublime Text.\nThe Sublime Text snap package could not be found.\n";
      else
        printf "\n\nInstalling Sublime Text via snap.\nSudo privileges are required to install Sublime Text.\n";
        sudo snap install --classic sublime-text
        add_themes=yes
      fi
    fi
  fi
  # Install Sublime Text Color Schemes
  sub_installed=$(which subl);
  if [[ -f "$HOME/.config/sublime-text-3/Packages/Color Scheme - Default/FullDark.sublime-color-scheme" ]]; then
    printf "FullDark color scheme is already installed.\n";
  elif [[ $sub_installed && -f "$HOME/Documents/Customize/FullDark.sublime-color-scheme" && -d "$HOME/.config/sublime-text-3" && "$add_themes" == "yes" ]]; then
    if ! [ -d "$HOME/.config/sublime-text-3/Packages/Color Scheme - Default" ]; then
      mkdir -p "$HOME/.config/sublime-text-3/Packages/Color Scheme - Default"
    fi
    if [ -d "$HOME/.config/sublime-text-3/Packages/Color Scheme - Default" ]; then
      rsync -rv "$HOME/Documents/Customize/FullDark.sublime-color-scheme" "$HOME/.config/sublime-text-3/Packages/Color Scheme - Default/"
      if [ -f "$HOME/.config/sublime-text-3/Packages/Color Scheme - Default/FullDark.sublime-color-scheme" ]; then
        printf "FullDark color scheme has been installed.\n";
      fi
    else
      printf "Unable to install FullDark color scheme.\nCheck ~/scripts/newPC_setup.sh\n\n";
    fi
  elif [[ $sub_installed && -f "$HOME/Documents/Customize/FullDark.sublime-color-scheme" && -d "$HOME/.config" ]]; then
    mkdir -p "$HOME/.config/sublime-text-3/Packages/Color Scheme - Default";

    if [ -d "$HOME/.config/sublime-text-3/Packages/Color Scheme - Default" ]; then
      rsync -rv "$HOME/Documents/Customize/FullDark.sublime-color-scheme" "$HOME/.config/sublime-text-3/Packages/Color Scheme - Default/"
      if [ -f "$HOME/.config/sublime-text-3/Packages/Color Scheme - Default/FullDark.sublime-color-scheme" ]; then
        printf "FullDark color scheme has been installed.\n\n";
      fi
    else
      printf "Unable to install FullDark color scheme.\nCheck ~/scripts/newPC_setup.sh\n\n";
    fi
  else
    printf "Unable to install the FullDark color scheme.\nCheck ~scripts/newPC_setup.sh\n\n";
  fi

# Install PacMan Theme into Default Theme
# This isn't currently working
# ........will need to troubleshoot
cd "$HOME/.config"
has_pacman_theme=$(grep -s -RF 'Pac Man Theme') > /dev/null
cd "$HOME"

  if [[ -z $has_pacman_theme || $has_pacman_theme == '' ]]; then
    printf "\n\nInstalling the Pac Man Theme.....\n\n";
    if ! [ -d "$HOME/.config/sublime-text-3/Packages/User" ]; then
      mkdir -p "$HOME/.config/sublime-text-3/Packages/User"
    fi
    if [ -f "$HOME/.config/sublime-text-3/Packages/User/Default.sublime-theme" ]; then
      mv "$HOME/.config/sublime-text-3/Packages/User/Default.sublime-theme" "$HOME/.config/sublime-text-3/Packages/User/Default.sublime-theme.bak"
    fi
    if ! [ -d "$HOME/.config/sublime-text-3/Packages/User/icons" ]; then
      mkdir -p "$HOME/.config/sublime-text-3/Packages/User/icons"
    fi
    if [ -f "$HOME/Documents/Customize/SublimeText/home/joe/.config/sublime-text-3/Packages/User/PacMan-Theme/PacMan.sublime-theme" ]; then
      rsync -rv "$HOME/Documents/Customize/SublimeText/home/joe/.config/sublime-text-3/Packages/User/PacMan-Theme/PacMan.sublime-theme" "$HOME/.config/sublime-text-3/Packages/User/Default.sublime-theme"
      rsync -rv "$HOME/Documents/Customize/SublimeText/home/joe/.config/sublime-text-3/Packages/User/PacMan-Theme/*.png" "$HOME/.config/sublime-text-3/Packages/User/icons/"
      rsync -rv "$HOME/Documents/Customize/SublimeText/home/joe/.config/sublime-text-3/Packages/User/PacMan-Theme/icons/" "$HOME/.config/sublime-text-3/Packages/User/icons/"
      printf "Pac Man Theme has been installed.\n\n";
    fi
  fi
else
  printf "Sublime Text is already installed.\n";
fi

# Add Sublime Text default key bindings
subl_installed=$(which subl)
if ! [[ -z $subl_installed || $subl_installed == '' ]]; then
  if [ -f "$HOME/Documents/Customize/SublimeText/home/joe/.config/sublime-text-3/Packages/User/Default (Linux).sublime-keymap" ]; then
    if ! [ -f "$HOME/.config/sublime-text-3/Packages/User/Default (Linux).sublime-keymap" ]; then
      rsync -rv "$HOME/Documents/Customize/SublimeText/home/joe/.config/sublime-text-3/Packages/User/Default (Linux).sublime-keymap" "$HOME/.config/sublime-text-3/Packages/User/"
    fi
  fi
fi

cd "$HOME"

# Install Spotify for Linux
has_spotify=$(which spotify)
if [[ -n "$has_spotify" && "$has_spotify" != '' ]]
then
  printf "Spotify is already installed.\n";
else
  if [[ -n "$has_zenity" && "$has_zenity" ]]
  then
      zenity --question --title="Spotify" --text="Would you like to install Spotify?" --timeout=6

    if (( $? == 0 ))
    then
      install_spotify=yes
    else
      printf "Skipping Spotify installation.\n\n";
      install_spotify=no
    fi
  else
    install_spotify=yes
  fi

  has_curl=$(which curl)
  if [[ "$install_spotify" == "yes" ]]; then
    if [[ "$snap_found" && "$snap_found" != "" ]]; then
      printf "\nInstalling Spotify using snap......\n\n";
      snap install spotify
    else
      if [[ -z "$has_curl" || "$has_curl" == '' ]]; then
        printf "Installing spotify will require installing curl.\nInstalling curl now......\n\n";
        sudo apt-get install curl
        printf "\n\n\n";
      fi

      has_curl=$(which curl)

      if [[ -n "$has_curl" && "$has_curl" != "" ]]; then
        printf "Installing Spotify via custom repository....\n\n";
        curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | sudo apt-key add - 
        echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
        sudo apt-get update && sudo apt-get install spotify-client
      else
        printf "Installation of curl failed......Aborting installation of Spotify.\n\n\n";
      fi
    fi
    printf "\n\n\n";
  fi
fi

# Install LAMP stack

has_apache=$(which apache2)
has_mysql=$(which mysql)
has_php=$(which php)

if [[ -n "$has_apache" && "$has_apache" != '' && -n "$has_mysql" && "$has_mysql" != '' && -n "$has_php" && "$has_php" != '' ]]
then
  printf "L.A.M.P. stack is already installed.\n";
else
  if [[ -n "$has_zenity" && "$has_zenity" ]]
  then
    zenity --question --title="LAMP Installation" --text="Would you like to install the full L.A.M.P. stack?" --timeout=6
    if (( $? == 0 ))
    then
      install_lamp=yes
    else
      install_lamp=no
    fi
  fi

  if [[ -n "$has_apache" && "$has_apache" != '' ]]
  then
   printf "Apache is already installed\n";
  else
    if [[ -n "$install_lamp" && "$install_lamp" == "yes" ]]
    then
      install_apache=yes
    elif [[ -n "$has_zenity" && "$has_zenity" ]]
    then
        zenity --question --title="Apache" --text="Would you like to install Apache?" --timeout=6

     if (( $? == 0 ))
     then
        install_apache=yes
     else
       install_apache=no
     fi
   else
     install_apache=yes
    fi

   if [[ "$install_apache" == "yes" ]]
   then
      printf "Updating Linux and installing Apache...\n\n";
      sudo apt update
      sudo apt install apache2

      printf "\n\n\nAdding server name to Apache config....";
      sudo printf "\n\n# Suppress global FQDN warning message\nServerName localhost" >> /etc/apache2/apache2.conf
   else
      printf "Skipping Apache installation.\n";
    fi
  fi

  if [[ -n "$has_php" && "$has_php" != '' ]]
  then
   printf "PHP is already installed\n";
  else
    if [[ -n "$install_lamp" && "$install_lamp" == "yes" ]]
    then
      install_php=yes
    elif [[ -n "$has_zenity" && "$has_zenity" ]]
    then
      zenity --question --title="PHP" --text="Would you like to install PHP?" --timeout=6

      if (( $? == 0 ))
      then
        install_php=yes
      else
        install_php=no
      fi
    else
     install_php=yes
    fi

    if [[ "$install_php" == "yes" ]]
    then
      sudo apt install -y php libapache2-mod-php php-mysql
      sudo apt install php-curl php-gd php-json php-mbstring php-xml

      has_pear=$(which pear)

      # TBD - This will proceed regardless if PEAR is installed or not....

      if [[ -z "$has_pear" || "$has_pear" == "" ]]
      then
        printf "PEAR package manager is not installed.\n\n";
      else
        printf "PEAR package manager is installed. \n";
      fi

      printf "Installing PEAR packages for PHP....\n\n";
      printf "Installing PEAR Mail package.\n\n";

      pear install Mail

      printf "Installing PEAR Mail_Mime package.\n\n";

      pear install Mail_Mime

      printf "Installing PEAR Net_SMTP package.\n\n";

      pear install Net_SMTP

      printf "PHP installation is finished.\n\nPHP version: ";

      php -v

      printf "\n\n\n";
    else
      printf "Skipping installation of PHP.\n";
    fi
  fi

  if [[ -n "$has_mysql" && "$has_mysql" != '' ]]
  then
   printf "MySQL is already installed\n";
  else
    if [[ -n "$install_lamp" && "$install_lamp" == "yes" ]]
    then
      install_mysql=yes
    elif [[ -n "$has_zenity" && "$has_zenity" ]]
    then
        zenity --question --title="MySQL" --text="Would you like to install MySQL?" --timeout=6

     if (( $? == 0 ))
     then
        install_mysql=yes
     else
       install_mysql=no
     fi
   else
     install_mysql=yes
    fi

   if [[ "$install_mysql" == "yes" ]]
   then
      printf "Intalling MySQL server....\n\n";
     sudo apt install mysql-server
   else
      printf "Skipping MySQL installation.\n";
    fi
  fi

  has_mysql=$(which mysql)
  has_php=$(which php)

  if [[ -n "$has_mysql" && "$has_mysql" != "" && -n "$has_php" && "$has_php" != "" && -n "$has_zenity" && "$has_zenity" ]]
  then
      zenity --question --title="PHP My Admin" --text="Would you like to install phpMyAdmin?" --timeout=6

    if (( $? == 0 ))
    then
       install_phpadmin=yes
    else
      install_phpadmin=no
    fi

    if [[ "$install_phpadmin" == "yes" ]]
    then
      printf "Installing phpMyAdmin....\n\n";
      sudo apt install phpmyadmin

      apache_has_phpmyadmin=$(grep -iF 'phpmyadmin' /etc/apache2/apache2.conf)

      if [[ -z "$apache_has_phpmyadmin" ]]
      then
        printf "\n\n\nAdding PHP My Admin to Apache config....\n\n";
        sudo printf "\n\n# PHP My Admin\nInclude /etc/phpmyadmin/apache.con" >> /etc/apache2/apache2.conf
      fi
    fi
  fi

  # Host config for fastclosures.test

  has_fast_conf=$(find /etc/apache2/sites-available/ -iname "*fastclosures*")
  if [[ -z "$has_fast_conf" ]]
  then
    if [[ -n "$has_zenity" && "$has_zenity" != '' ]]
    then
      zenity --question --title="Host Config" --text="Would you like to setup the Virtual Host configuration for fastclosures.test?" --width=250 --timeout=6

      if (( $? == 0 ))
      then
        setup_fastclosures=yes
      else
        setup_fastclosures=no
      fi

      if [[ "$setup_fastclosures" == "yes" ]]
      then
        sudo cp -r "$HOME/scripts/virtualhosts/fastclosures.test.conf" /etc/apache2/sites-available/fastclosures.test.conf
        sudo chown -R root:"$USER" /etc/apache2/sites-available/fastclosures.test.conf
        cd /etc/apache2/sites-available
        sudo a2ensite fastclosures.test.conf
        sudo systemctl reload apache2
        sudo apache2ctl  graceful

        printf "\n\nAdding fastclosures.test to /etc/hosts\n";
        sudo -- sh -c "sed -i \"2i127.0.1.1\tfastclosures.test\twww.fastclosures.test\" /etc/hosts"
      fi
    fi
  fi
fi

cd "$HOME"

# Install Node.js

has_nodejs=$(which node)
if [[ -z "$has_nodejs" ]]
then
  if [[ -n "$has_zenity" && "$has_zenity" != '' ]]
  then
    zenity --question --title="Install Node.js" --text="Would you like to install Node.js?" --width=250 --timeout=6

    if (( $? == 0 ))
    then
      install_node=yes
    else
      install_node=no
    fi
  else
    install_node=yes
  fi

  if [[ "$install_node" == "yes" ]]
  then
    printf "\n\nInstalling Node.js....\n\n";

    sudo apt install -y nodejs

    printf "\n\n\n\n";
  fi
else
  printf "Node.js is already installed\n";
fi

# Install npm
has_npm=$(which npm)
if [[ -z "$has_npm" ]]
then
  if [[ -n "$has_zenity" && "$has_zenity" != '' ]]
  then
    zenity --question --title="Install npm" --text="Would you like to install npm?\n\n(node package manager)" --width=250 --timeout=6

    if (( $? == 0 ))
    then
      install_npm=yes
    else
      install_npm=no
    fi
  else
    install_npm=yes
  fi

  if [[ "$install_npm" == "yes" ]]
  then
    printf "\n\nInstalling npm";

    sudo apt install -y npm

    printf "\n\n\n\n\n";
  fi
else
  printf "npm is already installed\n";
fi


cd "$HOME"

# Add Wallpaper Cron Job
has_cron=$(crontab -l | grep 'random_wallpaper.sh')

if [[ -z "$has_cron" || "$has_cron" == '' ]]; then
  printf "\n\nInstalling Wallpaper to Crontab.\n\n";
  crontab -l > temp_crontab_file
  echo "*/29  *   *   *   *   $HOME/scripts/random_wallpaper.sh $HOME/Wallpapers" >> temp_crontab_file
  crontab temp_crontab_file
  rm -f temp_crontab_file
else
  printf "Wallpaper Cron job already installed.\n";
fi

# Install MongoDB, Compass
# Install LAMP stack
# Install journal
# Install Microsoft Teams
# Install FileZilla

# This does not work, so comment-it out
has_teams=$(which teams)
if [[ -z $has_teams || $has_teams == '' ]]; then
	false;
#  cd $HOME/Downloads
#  wget -O $HOME/Downloads/microsoft_teams.deb https://go.microsoft.com/fwlink/p/?LinkID=2112886&clcid=0x409&culture=en-us&country=US
#  sudo dpkg -i $HOME/Downloads/microsoft_teams.deb
#  rm -rf $HOME/Download/microsoft_teams.deb
#  cd $HOME
fi

# Install other helpful tools
sshfs_inst=$(which sshfs)
tree_inst=$(which tree)
evince_inst=$(which evince)
xournal_inst=$(which xournal)
openconnect_inst=$(which openconnect)

if [[ -z "$sshfs_inst" || "$sshfs_inst" == '' ]]; then
  printf "\n\nInstalling sshfs......\n\n";
  sudo apt install sshfs
fi

if [[ -z "$tree_inst" || "$tree_inst" == '' ]]; then
  printf "\n\nInstalling tree utility....\n\n";
  sudo apt install -y tree
else
  printf "Tree command is already installed\n";
fi

if [[ -z "$evince_inst" || "$evince_inst" == '' ]]; then
  printf "\n\nInstalling evince utility....\n\n";
  sudo apt install -y evince
else
  printf "evince is already installed\n";
fi

if [[ -z "$xournal_inst" || "$xournal_inst" == '' ]]; then
  printf "\n\nInstalling xournal ulitiy.....\n\n";
  sudo apt install -y xournal
else
  printf "xournal is already installed\n";
fi

if [[ "$openconnect_inst" ]]; then
  printf "openconnect is already installed\n";
elif [[ ( -z "$openconnect_inst" || "$openconnect_inst" == '' ) && -d "$HOME/.vpn"  ]]; then
  printf "\n\nInstalling openconnect........\n\n";
  sudo apt install openconnect
else
  printf "\n\nVPN error: The ~/.vpn directory must exist before setting up the vpn connection.\n\n.";
fi

# Custom Keyboard Shortcuts
if [[ -n "$has_zenity" && "$has_zenity" != '' ]] && [ -f "$HOME/scripts/add_keyboard_shortcut.sh" ]
then
  zenity --question --title="Custom Keybindings" --text="Would you like to add the custom Shutdown Keyboard shortcut?" --width=250 --timeout=6

  if (( $? == 0 ))
  then
    config_shorcut1=yes
  else
    config_shorcut1=no
  fi

  if [[ "$config_shorcut1" == "yes" ]]
  then
    printf '\n\n\n\n\n\n';
    source "$HOME/scripts/add_keyboard_shortcut.sh"
    add_keyboard_shortcut "Custom Shutdown" "gnome-session-quit --power-off" "<Control><Alt>Delete"
    printf "\n\n\n****** Warning *******\n\n    You may need to remove the current Keyboard Shortcut for Log out before adding this shortcut.\n    Otherwise, it will block this new shortcut.   (Control+Shift+Delete)\n\n**********************\n\n\n";
  fi

  zenity --question --title="Custom Keybindings" --text="Would you like to add the custom Keyboard shortcuts to toggle the Built-in Display On/Off?" --width=380 --timeout=6

  if (( $? == 0 ))
  then
    config_shorcut=yes
  else
    config_shorcut=no
  fi

  if [[ "$config_shorcut" == "yes" ]]
  then
    printf '\n\n\n\n\n\n';
    source "$HOME/scripts/add_keyboard_shortcut.sh"
    add_keyboard_shortcut "Turn off Built-in Display" "$HOME/scripts/toggle_monitor.sh off" "<Shift><Ctrl>F8"
    add_keyboard_shortcut "Turn on Built-in Display" "$HOME/scripts/toggle_monitor.sh on" "<Shift><Ctrl>F9"
  fi
fi

# Install the Sleep Reminder cron job

if [[ ! -z "$has_zenity" && "$has_zenity" ]]
then
  zenity --question --title="Install Sleep Reminder" --text="Would you like to install the Sleep Reminder?" --timeout=6

  if (( $? == 0 ))
  then
    install_cron_job=yes
  fi
else
  read -p "Would you like to install the Sleep Reminder?" install_cron_job


  if [[ "$install_cron_job" =~ ^[yY] ]]
  then
    install_cron_job=no_zenity
  fi
fi

#  Zenity is required for the sleep reminder, because that is how
#  the user is notified to go to sleep.

if [[ "$install_cron_job" == 'no_zenity' ]]; then
  printf "Zenity is required to install the sleep reminder.\n\nE.g.,\n\tsudo apt-get install zenity\n\n";
elif [[ "$install_cron_job" == 'yes' ]]
then
  sudo_has_cron=$(sudo crontab -l | grep -Po "systemctl suspend" -)
  if [[ -z "$sudo_has_cron" || "$sudo_has_cron" == '' ]]; then
    # Install the cron job to suspend for the root user
    sudo crontab -l > sudo.crontab.temp
    sudo echo "45   21   *   *   0,1,2,3,4,5     /usr/bin/systemctl suspend" >> sudo.crontab.temp
    sudo crontab sudo.crontab.temp
    sudo rm -f sudo.crontab.temp

    # Install the cron job for the reminder for the user
    crontab -l > my.crontab.temp
    echo "40   21   *   *   0,1,2,3,4,5     zenity --title=\"Go to sleep\" --error --text=\"You need to go to bed.\nSuspending in 5 minutes.\" --width=350 --display=:0.0" >> my.crontab.temp
    crontab my.crontab.temp
    rm -f my.crontab.temp
   else
    printf "The sleep-minder cron job is already installed.\n\n";
  fi
else
  printf "Skipping installation of the sleep reminder.";
fi


printf "\n\n\nInstallation has finished!!!\n\n";
