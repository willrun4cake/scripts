#!/bin/bash

# Toggles between 2 HDMI monitors ONLY

# This script is designed to provide programmatic KVM switching for a VCP-enabled moniter
# It cycles between HDMI-1, HDMI-2, and DisplayPort1
# Since I'm unaware how to detect a list of all connected inputs to the monitor,
# the best I can do is toggle between hard-coded input numbers

# Currently only HDMI-1 (0x11) and HDMI-1 (0x12), DisplayPort (0x0f) are supported

# You will need the current user added to the i2c group to execute this
# script as a non-root user,
#
#    sudo usermod your-user-name -aG i2c
#    shutdown -r now
#
#    (This requires a restart to take effect)

# You may also need to add /usr/bin/ddcutil to the sudoers file
#
#  Defaults        secure_path="/yadda/yadda:/yadda/yadda:/usr/bin/ddcutil"

# Refer to below in case this needs to be run as root user:
# uuid=$(id -u)

#if [ ! "$uuid" -eq 0 ]; then
#  printf "You are not running as root user\nTry running with sudo.\n\n";
#  exit
#fi


ddcutil_installed=$(which ddcutil)

if [[ -z "$ddcutil_installed" ]]
then
  printf "You need to install ddcutil to using this feature.\n    sudo apt install ddcutil\n\n\n";
  zenity --notification --title="Error" --text="You need to install ddcutil to using this feature:\n\n       sudo apt install ddcutil\n\n\n";
  exit
fi

# Get Monitor Display number
#m1=$(ddcutil detect | grep -oE '^Display ([[:digit:]]{1,6})$')
m1='Display 1'

if [[ $(printf "%s" "$m1" | grep -oiE 'Display [[:digit:]]{1,6}') ]]
then
#  m1num=$(printf "%s" "$m1" | grep -oiE '[[:digit:]]{1,6}$')
m1num=1

  if [[ -z "$m1num" ]]
  then
    printf "Primary monitor was not detected. (%s)\n\n" "$m1num";
    exit
  fi
fi

printf "Detected first Display: %s (%s)\n\n\n" "$m1" "$m1num"

 selected_input_str=$(ddcutil getvcp 0x60 -d "$m1num" | grep -oiE '=0x[0-9a-fA-F]{1,6}')

if [[ -z "$selected_input_str" ]]
then
  printf "Unable to identify current display input on Display #%s\n\ndebug string: %s" "$m1num" "$selected_input_str";
  exit
fi

selected_input=$(printf "%s" "$selected_input_str" | grep -ioE '0x[0-9a-fA-F]{1,6}')

printf "Selected input: %s\nDebug str: %s\n\n" "$selected_input" "$selected_input_str"

if [[ "$selected_input" == "0x0f" ]] || [[ "$selected_input" == "0x0F" ]] || [[ "$selected_input" == "0x11" ]] || [[ "$selected_input" == "0x12" ]]
then
  if  [[ "$selected_input" == "0x11" ]]
  then
    printf "Switching from %s to %s\n\n\nDegbug str: %s\n\n" "0x11" "0x12" "$selected_input_str"
    ddcutil setvcp 0x60 0x12 -d "$m1num"
  elif [[ "$selected_input" == "0x12" ]]
  then
    printf "Switching from %s to %s\n\n\nDegbug str: %s\n\n" "0x12" "0x0f" "$selected_input_str"
    ddcutil setvcp 0x60 0x0f -d "$m1num"
  elif [[ "$selected_input" == "0x0F" ]] || [[ "$selected_input" == "0x0f" ]]
  then
    printf "Switching from %s to %s\n\n\nDegbug str: %s\n\n" "0x0f" "0x11" "$selected_input_str"
    ddcutil setvcp 0x60 0x11 -d "$m1num"
  else
    printf "\n\nUnidentified input is selected: %s\n\nDebug str: %s\n\n" "$selected_input" "$selected_input_str"
  fi
else
  printf "\n\nCould not determine a valid input identifier.\n\nSelected input: %s\n\n\n" "$selected_input"
fi

zenity --notification --text="Switching from input: $selected_input"
