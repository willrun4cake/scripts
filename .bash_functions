#!/bin/bash

# Bash custom user functions (to be invoked from .bashrc)

# Function to abbreviate grep non-recursive search
# If first argument is . then perform a flat (non-recursive) search
# If 1st or 2nd argument is numeric then this becomes lines of context

function search() {
  if [[ -z "$1" ]]
  then
    echo "You did not enter a search argument.\n\n"
    return -1;
  elif ! [[ -z "$4" ]]
  then
    printf "Enter just 1 search argument.\n\n\n";
    return -1;
  elif ! [[ -z "$1" ]] && ! [[ -z "$2" ]] && ! [[ -z "$3" ]] && [[ "$1" == '.' ]] && [[ "$2" =~ ^[0-9]+$ ]]
  then
    if [[ "$2" =~ ^[01]$ ]]
    then
      grep -C 0 -iF "$3" *
    else
      grep -C "$2" -iF "$3" *
    fi
  elif ! [[ -z "$1" ]] && ! [[ -z "$2" ]] && ! [[ -z "$3" ]]
  then
    printf "Enter only a single search argument.\n\n\n";
    return -1;
  elif ! [[ -z "$1" ]] && ! [[ -z "$2" ]] && [[ "$1" == '.' ]] && [[ -z "$3" ]]
  then
    printf "Performing flat search (non-recursive) for: %s\n\n\n" "$2"
    grep -C 4 -iF "$2" *
  elif ! [[ -z "$1" ]] && ! [[ -z "$2" ]] && ! [[ "$1" =~ ^[0-9]+$ ]]
  then
    echo "Argument 1 must be numeric.\nIt denotes lines of context.";
  elif ! [[ -z "$1" ]] && ! [[ -z "$2" ]] && [[ "$1" =~ ^[0-9]+$ ]]
  then
    if ! [[ -z "$1" ]] && ! [[ -z "$2" ]] && [[ "$1" =~ ^[01]$ ]]
    then
      grep -C 0 -iRF "$2" .
    else
      grep -C "$1" -iRF "$2" .
    fi
  elif ! [[ -z "$1" ]] && [[ -z "$2" ]]
  then
    grep -C 3 -iRF "$1"
  else
    echo 'You need to specify a numeric number of lines of context.';
    return -1;
  fi
}

# Copy file contents to clipboard
function copy ()
{
  xclip_installed=$(which xclip);

  if [[ ! -z "$xclip_installed" ]]
  then
    xclip -selection clipboard "$@";

    printf "\n\nFile contents copied.\n\n";
  else
    printf "\n\nxclip is not installed.\n\nInstall xclip first:\n\n\n    sudo apt-get install xclip\n\n";
  fi
}

# Create an image file on disk from the clipboard
# Must install xclip first
function pasteimage() {
        set -e
        printf "Save filename as (no extension):\n"
        read -r name
        if [ -z ${name} ]
        then
          echo "You did not enter a file name. Terminating...."
          exit 1
        fi

	set +e
        xclip -selection clipboard -t image/png -o > ${name}.png
}

# Shuffle wallpaper manually
if [ -f /home/joe/scripts/random_wallpaper.sh ]; then
  function shuffle() {
    /home/joe/scripts/random_wallpaper.sh /home/joe/Wallpapers
  }
fi

# Rotate PDF documents using a wrapper for PDFTK
if [ -f /home/joe/scripts/rotatePdf.sh ]; then
  echo "Do nothing. The official package should be used going forward, not this script." > /dev/null
 #  source /home/joe/scripts/rotatePDF/rotate_pdf_failover.sh
fi

# Python aliases
# Add "python" as an alternative with priority 5
# Effectively aliasing the full python + version to just "python", e.g.,
#  python -> python3.10

short_python=$(which python)
short_python="xxxxxx";
if [[ -z "${short_python}" ]]; then
  version_2digit=$(python3 -V | grep -oP "(?<=python)[[:digit:]]\.[[:digit:]]+" -)
  echo -e "Aliasing python to python${version_2digit}\n\n"
  sudo update-alternatives --install /usr/bin/python python /usr/bin/python"${version_2digit}" 5
  echo "Done.";
fi

# Helper function to check if the windowing system is X11 or Wayland
function disp() {
  sessionNumber=$(loginctl | grep -Po "(?<=[ \t])+[\d]+" - | head -1)
  if [[ ! -z "$sessionNumber" ]]; then
    windowingSystem=$(loginctl show-session "${sessionNumber}" | grep -Po "(?<=Type\=)[\\w]+" -)
    echo "${windowingSystem}";
  else
    echo "Unable to identify the windowing system on this GUI.";
  fi
}

# SCT if missing
has_sct=$(which sct)
has_xsct=$(which xsct)
if [[ -z "${has_sct}" ]] && [[ "${has_xsct}" ]]; then
  function sct () {
    xsct "$@"
  }
fi
