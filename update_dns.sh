#!/bin/bash

# For testing purposes, you can...
# Reset the IP to something meaningless by navigating to the below URL in a browser
# https://hotmomsclub:side%2Fb00b@freedns.afraid.org/nic/update?hostname=new.ovationuploads.com&myip=99.99.99.99

printf "\nYou are running a Dynamic DNS update script....\n\n";

PUBLIC_IP=`wget http://ipecho.net/plain -O - -q ; echo`
printf "Updating IP address to:\n\n   %s\n\n\n" $PUBLIC_IP;

# Array of domains, usenames, passwords
domain_encoded=( foreclosuresdb.com ovationuploads.com apples.com bananas.com )
username_encoded=( hotmomsclub user1 user2 )
pwd_encoded=( side%2Fb00b myPassword1 myPassword2 )

arlength=${#domain_encoded[@]}

printf "Total number of domains to update: %d\n\n" $arlength

for ((i=0; i < ${#domain_encoded[@]}; ++i))
do
  printf "Updating domain # %s:\n  Username: %s\n  Password: %s\n  Domain: %s\n\n\n" $((i+1)) "${username_encoded[$i]}" "${pwd_encoded[$i]}" "${domain_encoded[$i]}";
  curl -v --connect-timeout 8 --max-time 8 --header "Connection: close" "https://${username_encoded[$i]}:${pwd_encoded[$i]}@freedns.afraid.org/nic/update?hostname=${domain_encoded[$i]}&myip=$PUBLIC_IP";
  printf "\n\n\n\n\n\n";
  # If upating just one domain:
  # curl -v --connect-timeout 8 --max-time 8 --header "Connection: close" "https://$username_encoded:$pwd_encoded@freedns.afraid.org/nic/update?hostname=$domain_encoded&myip=$PUBLIC_IP";
done

# For testing in the terminal
#sleep 2
#printf "\nFinished.......\n\n";
