#!/bin/bash
place=$(zenity --width=150 height=90 --entry --title="Do the thing" --text="Where do you want to look for your stuff?" 2>/dev/null)
if [ "$place" ]
then
  # Replace ~ with home directory if it occurs
  place="${place//\~/$HOME}"
  echo "New place: $place"
  if [[ -d $place ]] || [[ -f "${place}" ]]
  then
    stuff=$(cd $place; ls)
    zenity --width=230 height=280 --info --title="Your Stuff" --text="Here is your stuff at\n$place:\n\n $stuff" 2>/dev/null
  else
    zenity --width=180 height=80 --error --title="Error" --text="That is not a valid location." 2>/dev/null
  fi
else
  zenity --width=110 height=90 --error --title="Error" --text="You did not enter a place." 2>/dev/null
fi

zenity --question --title "Feedback" --text="Did you enjoy this session?" --timeout=4

if (( $? == 0 ))
then
  echo "Yes, you enjoyed the session.";
else
  echo "No, you either did not enjoy the session or timed out.";
fi
