# Initialize functions for GPU control
if [[ -f ~/scripts/graphics_card.sh ]]; then
  function gpu() {
    . ~/scripts/graphics_card.sh $1 $2 $3 $4 $5
  }
fi

# Function for GPU fan control
has_atitool=`echo $PATH | grep -i 'atitool'`
has_ati_tool=`echo $PATH | grep -i 'ATI-Tool'`
if [[ -z $has_atitool && $has_atitool == '' && -z $has_ati_tool && $has_ati_tool == '' && -d $HOME/ATI-Tool ]]; then
  export PATH=$PATH:$HOME/ATI-Tool
fi

# Need to check $PATH for sudo as well.
# If not there, will need to edit /etc/sudoers file using visudo

# Change
#     Defaults        secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin"
# To
#     Defaults        secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:/home/joe/ATI-Tool"
#


no_find_atitool=`type atitool | grep 'not found'`
if [[ ! $no_find_atitool &&  -f ~/scripts/gpu_fan.sh ]]; then
  function fan() {
    sudo /bin/bash ~/scripts/gpu_fan.sh $1 $2 $3 $4 $5
  }
else
  if [[ $no_find_atitool && ! -f ~/scripts/gpu_fan.sh ]]; then
    printf "ATI Tool status:\n  atitool is not installed\n  gpu_fan.sh script not found"
  elif [[ $no_find_atitool ]]; then
    printf "ATI Tool status: atitool is not installed"
  elif [[ ! -f ~/scripts/gpu_fan.sh ]]; then
    printf "ATI Tool status: ~/scripts/gpu_fan.sh not found"
  else
    printf "Unable to install the fan tool.\n\nCheck .bash and ~/scripts/gpu_fan.sh"
  fi
fi
