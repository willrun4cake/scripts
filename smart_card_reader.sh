#!/bin/bash

uuid=$(id -u)

if [ ! "$uuid" -eq 0 ]; then
  printf "You are not running as root user\nTry running with sudo.\n\n";
  exit
fi

has_pcscd=$(which pcscd)

if [[ ! "$has_pcscd" || "$has_pcscd" == '' || -z "$has_pcscd" ]]; then
  printf "pcscd is not installed.\nTerminating......\n\n";
  exit
fi

is_running=$(sudo systemctl status pcscd)
inactive_token=$(echo "$is_running" | grep -iF 'inactive')
running_token=$(echo "$is_running" | grep -iF 'Active: active')

if [[ -n "$inactive_token" ]]; then
  printf "Starting pcscd daemon.\n";
  sudo systemctl start pcscd
elif [[ -n "$running_token" ]]; then
  printf "The pcscd daemon is ready.\n\t";
else
  printf "Not sure if pcscd daemon is ready.\n\n";
fi

is_running=$(sudo systemctl status pcscd)
running_token=$(echo "$is_running" | grep -iF 'Active: active')

if [[ -z "$running_token" || "$running_token" == '' ]]; then
  printf "Unable to start pcscd daemon.\nTerminating program.......\n\n";
  exit
fi

printf "Starting Smart Card Reader    (scan process)\n\n";
pcsc_scan

