#!/bin/bash
# Get sleep time
timer=$(zenity --width=170 --height=90 --entry --title="Sleepy Time" --text="How long do you want to sleep?" 2>/dev/null)
if [ "$timer" ]
then
  sleep $timer
  zenity --width=140 --height=110 --info --title="Time is Up"  --text="The time is over.\n\nIt has been $timer seconds" 2>/dev/null
else
  zenity --width=140 --height=110 --info --title="Warning" --text="No valid time was entered" 2>/dev/null
fi
