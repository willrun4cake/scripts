#!/bin/bash

# This script requires the atitool fork
# developed by Patrick Shur.
#   https://github.com/patrickschur/atitool
#
# Enter below for the ATItool help menu:
#	sudo atitool
#
# Enter below to get ATItool Voltage/Current/Thermal/Fan control menu:
#	sudo atitool vctf --help

# Custom Fan control functions
uuid=`id -u`

if [[ ! uuid -eq 0 ]]; then
  printf "You are not running as root\nTry running with sudo."
  exit
fi

type_atitool=`type atitool | grep 'not found'`
if [[ $type_atitool ]]; then
  printf "The atitool was not found.\nTo use this feature you must install the atitool binary,\nand add it to the PATH variable."
  printf "https://github.com/patrickschur/atitool"
  exit
fi

# Future: Research if it's possible to configure a fan control profile
#         using multiple fan speed arguments.
if [[ $1 && $2 && $3 && $4 ]]; then
  # printf "fan $1 and $2 and $3 and $4"
  echo -e "You did not enter a valid argument list."
elif [[ $1 && $2 && $3 ]]; then
  # printf "fan $1 and $2 and $3"
  echo -e "You did not enter a valid argument list."
elif [[ $1 && $2 ]]; then
  # printf "fan $1 and $2"
  echo -e "You did not enter a valid argument list."
elif [[ $1 ]]; then
  if [[ $1 == 'auto' ]]; then
    atitool -fancontrol=2
  elif [[ $1 == 'lo' || $1 == 'low' ]]; then
    atitool -fancontrol=0
  elif [[ $1 == 'hi' || $1 == 'high' ]]; then
    atitool -fancontrol=1
  elif [[ $1 == 'med' || $1 == 'medium' ]]; then
    atitool -fancontrol=3
  elif [[ $1 =~ ^-[0-9]+$ ]]; then
    printf "You entered a negative fan speed: %s.\nOnly non-negative integers from 0 - 100 are allowed." $1
  elif [[ $1 =~ ^[0-9]{4,}$ ]]; then
    printf "You entered: %s.\nMAX fan speed is 100.\nYou must enter a number of 100 or less." $1
  elif [[ $1 =~ ^[0-9]{1,3}$ ]]; then
    speed=$(($1 + 0))
    if (( $speed > 100 )); then
      printf "Fan speed entered: %s\nThe MAX fan speed is 100 (100%%)" $1
    elif (( $speed == 100 )); then
      printf "atitool -fancontrol=100"
      atitool -fancontrol=100
    elif (( $speed > 19 && $speed < 100 )); then
      atitool -fancontrol=$1
    elif (( $speed > -1 && $speed < 20 )); then
      atitool -fancontrol=low
    else
      printf "You entered an invalid number: %s" $1
    fi
  else
    printf "You entered an invalid fan speed: %s\nPlease enter an INTEGER fan speed %% from 0 - 100.\n\nOR  enter auto, lo, med, or hi \n\n\n\n(Do NOT enter the percent sign (%%), just the integer value.)" $1
  fi
else
  atitool -vctfstatus
  printf "\n"
  atitool -temp | grep -E '[[:digit:]]{1,3}\.[[:digit:]]{0,8}[[:space:]]'
  printf "\n"
  atitool -fanrpm | grep -E '([[:space:]][[:digit:]]{2,6}[[:space:]])RPM'
fi
