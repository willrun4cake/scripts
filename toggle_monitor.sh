#!/bin/bash

if [[ "$1" == "on" ]] || [[ "$1" == "On" ]] || [[ "$1" == "ON" ]]; then
  toggle="on";
  title="Display On";
  message="Built-in display (eDP-1) has been re-activated by Xrandr.";
elif [[ "$1" == "off" ]] || [[ "$1" == "Off" ]] || [[ "$1" == "OFF" ]]; then
  toggle="off";
  title="Display Off";
  message="Built-in display (eDP-1) has been deactivated by Xrandr.";
fi

has_zenity=$(which zenity)

if [[ ! -z "${toggle+x}" ]] && [[ ! -z "$toggle" ]]; then
  has_xrandr=$(which xrandr)

  if [[ ! -z "$has_xrandr" ]]; then
    if [[ "$toggle" == "off" ]]; then
      xrandr --output eDP-1 --off
    elif [[ "$toggle" == "on" ]]; then
      xrandr --output eDP-1 --auto
    fi

    if [[ ! -z "$has_zenity" ]]; then
      zenity --info --title="$title" --text="$message" --timeout=8
    else
      echo -e "$message\n\n";
    fi
  else
    if [[ "$has_zenity" ]]; then
      zenity --info --title="Display On" --text="Xrandr must be installed to toggle the primary monitor on/off." --timeout=8
    else
      echo -e "Xrandr must be installed to toggle the primary monitor on/off.";
    fi
  fi
fi
