#!/bin/bash
# Mount Remote Filesystems
function mymount () {
  vpn_connection=$(pgrep -l openconnect)
  if [ ! -f "$HOME/.ssh/ntplms-super_passphrase.sh" ]; then
    if [ -d "$HOME/.ssh" ]; then
      printf "#\!\/bin\/sh\necho HelloKitty69" > "$HOME/.ssh/ntplms-super_passphrase.sh";
      chmod u+x "$HOME/.ssh/ntplms-super_passphrase.sh";
      chmod g+x "$HOME/.ssh/ntplms-super_passphrase.sh";
    else
      printf "No $HOME/.ssh directory exists. Terminating ssh key pre-setup....\n\n";
    fi
  fi
  if [[ "$1" == "idrive" ]]; then
    if [ ! -d /var/www/html/idrive ]; then
      mkdir -p /var/www/html/idrive
    fi
      fusermount -u /var/www/html/idrive &
      sleep 1
      sshfs -o allow_other zoe@idrive-compute-zoe:/var/www/html/ /var/www/html/idrive -o transform_symlinks
      if [ $? == 0 ]
      then
        printf "iDrive Compute has been mounted.\n\n"
      fi
  elif [[ "$1" == "ipage" ]]; then
    if [ ! -d /var/www/html/ipage ]; then
      mkdir -p /var/www/html/ipage
    fi
    DISPLAY=1 SSH_ASKPASS="$HOME/.ssh/ntplms-super_passphrase.sh" ssh-add "$HOME/.ssh/ntplms-super" < /dev/null;
    # Password should be HelloKitty69
      fusermount -u /var/www/html/ipage &
      sleep 1
      echo 'BigD@t@16' | sshfs -o allow_other pippin16@ipage:/ /var/www/html/ipage -o password_stdin -o transform_symlinks
      if [ $? == 0 ]
      then
        printf "iPage has been mounted.\n\n"
      fi
  elif [[ -z ${vpn_connection+x} || "$vpn_connection" = "" ]]; then
    echo "You need to start the VPN first."
  elif [[ "$1" == "microsite-test" ]]; then
    if [ -d /var/www/html/microsite-test ]; then
      fusermount -u /var/www/html/microsite-test &
      sleep 1
      rmdir /var/www/html/microsite-test
      mkdir /var/www/html/microsite-test
      sshfs -o allow_other root@test_microsite_super:/var/www/html /var/www/html/microsite-test
    else
      mkdir -p /var/www/html/microsite-test
      sshfs -o allow_other root@test_microsite_super:/var/www/html /var/www/html/microsite-test
    fi
  elif [[ "$1" == "microsite-dev" ]]; then
    if [ -d /var/www/html/microsite-dev ]; then
      fusermount -u /var/www/html/microsite-dev &
      sleep 1
      rmdir /var/www/html/microsite-dev
      mkdir /var/www/html/microsite-dev
      sshfs -o allow_other root@dev_microsite_super:/var/www/html /var/www/html/microsite-dev
    else
      mkdir -p /var/www/html/microsite-dev
      sshfs -o allow_other root@dev_microsite_super:/var/www/html /var/www/html/microsite-dev
    fi
  fi
}
