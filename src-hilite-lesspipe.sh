#! /bin/bash

# You will need the "source-highlight" package installed for this script to work
#     sudo apt-get install source-highlight

# Use my personal style file if it exists
if [ -f /usr/share/source-highlight/joe.style ]; then
	styleFile=/usr/share/source-highlight/joe.style
else
	styleFile=/usr/share/source-highlight/esc.style
fi

for source in "$@"; do
    case $source in
	*ChangeLog|*changelog) 
        source-highlight --failsafe -f esc --lang-def=changelog.lang --style-file="${styleFile}" -i "$source" ;;
	*Makefile|*makefile) 
        source-highlight --failsafe -f esc --lang-def=makefile.lang --style-file="${styleFile}" -i "$source" ;;
	*.tar|*.tgz|*.gz|*.bz2|*.xz)
        lesspipe "$source" ;;
        *) source-highlight --failsafe --infer-lang -f esc --style-file="${styleFile}" -i "$source" ;;
    esac
done
