#!/bin/bash
# Provide cron the necessary environment variables
PID=$(pgrep gnome-session | tail -n1) 2> /dev/null
export DBUS_SESSION_BUS_ADDRESS=$(grep -z DBUS_SESSION_BUS_ADDRESS /proc/$PID/environ | cut -d= -f2-) 2> /dev/null

script_path=`realpath $0`
if [ -z "$1" ]
then
  echo "Enter directory containing background images (no trailing slash): "
  read target_dir
else
  target_dir="$1"
fi
if [[ ${target_dir:0:1} == '~' ]]
then
  dir_length=${#target_dir}
  target_dir=$HOME/${target_dir:2:dir_length - 1}
fi
files=( )
printf "%s%s\n" "Using images in: " $target_dir
# image_extensions={'png','jpg','bmp','jpeg'}
image_extensions=( png jpg jpeg bmp tiff gif )
cd $target_dir
rm -f list990099.txt
for ext in ${image_extensions[@]}
do
  find $target_dir -iname "*.$ext" >> list990099.txt
done
readarray -t  files < list990099.txt
if [ -f $HOME/.used.images ]
then
  readarray -t used_images < $HOME/.used.images
  if [ ${#files[@]} = ${#used_images[@]} ]
  then
    rm -f $HOME/.used.images
    printf "%s\n" "Transitioned through all Wallpaper images. Resetting list."
  else
	# Omit background image if in the list of recently used images
	temp_array=()
	for file in "${files[@]}" 
	do
	  found=false
	  for image in $used_images
	  do
	    if [ "$file" == "$image" ]
	    then
	      found=true
	    fi
	  done
	  if [ "$found" = false ]
	  then
	    temp_array+=("$file")
	  fi
	done
	files=("${temp_array[@]}")
	unset temp_array
  fi
fi
chosen=`printf "%s" "${files[RANDOM % ${#files[@]}]}"` 
printf "%s\n" "Choosing file: $chosen"
printf "%s%s\n" "Choosing background image: " $chosen >> $HOME/.used.images
gsettings set org.gnome.desktop.background picture-uri "file:///$chosen"
# Issue the same command except for Dark theme, in case Dark theme is enabled
gsettings set org.gnome.desktop.background picture-uri-dark "file:///$chosen"
# rm -f list990099.txt
printf "%s%s\n" "this file is: " $script_path
# {crontab -l; echo "*	*	*	*	*	$script_path"} | crontab 
