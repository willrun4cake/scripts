#!/bin/bash

# Create function to connect to VPN if the necessary info exists
# This will needs sudo permissions for openconnect
if [ -d ~/.vpn ]; then
 if [ -f ~/.vpn/.static.pass ] && [ -f ~/.vpn/.username ] && [ -f ~/.vpn/.vpn.url ] ;  then
 VPN_USERNAME=`cat ~/.vpn/.username`
 VPN_PASSWD=`cat ~/.vpn/.static.pass`
 VPN_URL=`cat ~/.vpn/.vpn.url`
  if [[ ${#VPN_USERNAME} > 0 ]] && [[ ${#VPN_PASSWD} > 0 ]] && [[ ${#VPN_URL} > 0 ]] && [[ ${VPN_USERNAME:0:5} != "# Rep" ]] && [[ ${VPN_URL:0:5} != "# Rep" ]] && [[ ${VPN_PASSWD:0:5} != "# Rep" ]]; then
   if [ -f ~/scripts/vpn_connector.sh ]; then
    function vpn() {
     /bin/bash ~/scripts/vpn_connector.sh ${VPN_URL} ${VPN_USERNAME} ${VPN_PASSWD} $1
    }
    else
     echo -e 'The entries in your ~/.vpn directory could not be read.\n Unable to initialize vpn() function.'
     echo -e 'Please perform a git reset on your ~/.vpn directory.'
   fi
  fi
 else
  echo -e "\033[0;34m\nYour vpn connection has not yet been configured.\nEdit the files in ~/.vpn\033[0m"
  if ! [ -f ~/.vpn/.vpn.url ]; then
   echo "# Replace this line with your vpn's url (no other text or new lines)" > ~/.vpn/.vpn.url
   echo "VPN configuartion: You need to replace the default text in ~/.vpn/.vpn.url"
  fi
  if ! [ -f ~/.vpn/.username ]; then
   echo "# Replace this line with your username (no other text or new lines)" > ~/.vpn/.username
   echo "VPN Configuration: You need to replace the default text in /.vpn.username"
  fi
  if ! [ -f ~/.vpn/.static.pass ]; then
   echo "# Replace this line with your vpn password (no other text or new lines)" > ~/.vpn/.static.pass
   echo "VPN configuration: You need to replace the default text in ~/.vpn/.static.pass"
  fi
 fi
else
 mkdir -p ~/.vpn
 echo -e "\033[0;34m\nYour vpn connection has not yet been configured.\nEdit the files in ~/.vpn\033[0m"
  if ! [ -f ~/.vpn/.vpn.url ]; then
   echo "# Replace this line with your vpn's url (no other text or new lines)" > ~/.vpn/.vpn.url
  fi
  if ! [ -f ~/.vpn/.username ]; then
   echo "# Replace this line with your username (no other text or new lines)" > ~/.vpn/.username
  fi
  if ! [ -f ~/.vpn/.static.pass ]; then
   echo "# Replace this line with your vpn password (no other text or new lines)" > ~/.vpn/.static.pass
  fi
fi

