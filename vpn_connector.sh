#!/bin/bash

echo 'beginning vpn script'
VPN_URL="$1"
VPN_USERNAME="$2"
VPN_PASSWD="$3"
if ! [[ -z $4 ]]; then
 TWO_FACTOR_AUTH_PASS="$4"
else
 echo "Enter Google Authenticator password: "
 read TWO_FACTOR_AUTH_PASS
fi
echo "Connecting to VPN using the following credentials:"
echo -e "URL: ${VPN_URL}\nUsername: ${VPN_USERNAME}\nPassword1: ${VPN_PASSWD}\nPassword2: ${TWO_FACTOR_AUTH_PASS}\n"
nohup echo -e "${VPN_PASSWD}\n${TWO_FACTOR_AUTH_PASS}\n" | sudo /usr/sbin/openconnect -v -b -u ${VPN_USERNAME} ${VPN_URL} --servercert pin-sha256:nLp0AEC9Z3KAXWGBsTivRtFl6PG//NvIQg0/Ycmw1/Q= --passwd-on-stdin > /dev/null
echo ''
sleep 2.3
vpn_connection=$!
  #clear
  echo ''
  if ! [ -z ${vpn_connection+x} ]; then
    unset vpn_connection
  fi
  vpn_connection=$(pgrep -l openconnect)
  if [[ -z "$vpn_connection" ]]; then
    printf "Failed to connect to VPN."
  else
    printf "VPN connection:\n    %s" "$vpn_connection"
  fi

