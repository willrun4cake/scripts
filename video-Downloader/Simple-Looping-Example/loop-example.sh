#!/bin/bash

IFS=$'\n'

while read -r -a line; do
  if [[ -z "$line" ]]; then
    continue;
  fi
  IFS=',' read -r -a data <<< "$line"
  printf "Name: %s, Color: %s, Quantity: %d \n" "${data[0]}" "${data[1]}" "${data[2]}"
  # IFS=$'\n'
done < ./example-list.txt
