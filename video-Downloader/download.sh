#!/bin/bash

has_youtubedl="$(which youtube-dl)"

if [[ -z "$has_youtubedl" ]]; then
  printf "\nYou need to install youtube-dl to use this utility:\n\n\tsudo apt-get install youtube-dl\n\n\n";
  exit
fi

function trim() {
  if [[ -z "$1" ]]; then
    echo "$1"
    return 0;
  fi

  if [[ ! "$1" =~ ^[[:space:]]+ ]] && [[ ! "$1" =~ [[:space:]]+$ ]]; then
    echo "$1"
    return 0;
  fi

  echo "$(sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'<<<"$1")"
}

function initVideosList() {
  cat << 'EOF' > videos.list
https://video.com/video-1.wmv, My 1st Video, https://video.com/thumbnail-1.jpg, Thumbnail1.jpg, Andy Anderson; Billy Bailout; Charlie Cherrypicker, wmv, wmv

https://video.com/video-2.m4v, My 2nd Video, https://video.com/thumbnail-2.jpg, Thumbnail2.jpg, Morgan Friedman; Billie Eyelash; Casey Cashspot, m4v, m3u8

https://video.com/video-3.mkv, My 3rd Video, https://video.com/thumbnail-3.jpg, Thumbnail3.jpg, Allen Ablebody; Becky Bailliff; Chet Chillhouse, mkv, mkv
EOF

  chmod 664 videos.list
}

function validatePath() {
  exitOnFailedWrite=false
  printf "\n\nYou need to provide the video output path with write permission.\n";
  printf "Video file path:\n\t%s\n\n" "$1"
  printf "Attempting to add write permission now.\n";
  sudo chmod u+w "$1"
  if (( $? > 0 )); then
    printf "\n\n\nError: Unable to set user write permission for $1.\n\n";
  fi
  sudo chmod g+w "$1"
  if (( $? > 0 )); then
    printf "\n\n\nError: Unable to set group write permission for $1.\n\n";
  fi

  if [[ "$exitOnFailedWrite" =~ ^TRUE$|^YES$|^[Tt]rue$|^[Yy]es$ ]]; then
    printf "\n\n\nTerminating script because the videos cannot be placed in their target directory.\n\n\n";
    printf "Terminating......\n\n";
    exit
  else
    printf "\nSuccessfully set write permissions for ${VideosDirPath}.\n\n";
  fi

  mv "$fileName" "$fullFilePath"

  if (( $?  > 0 )); then
    mv "'$fileName'" "$fullFilePath"
  fi
}

function youtubeDl() {
  # Use file meta-data to create the file name
  useMetadata=false

  fileName="${Title}.${Extension}"
  fullFilePath="${VideosDirPath}/${fileName}"

  trailerFileName="'${Title}.trailer.${Extension}'"
  trailerFullFilePath="$TrailersDirPath/'${Title}.trailer.${Extension}'"

  previewFileName="'${Title}.preview.${Extension}'"
  previewFullFilePath="$PreviewsDirPath/'${Title}.preview.${Extension}'"

  if [[ -f "$fullFilePath" ]]; then
    printf "\nThe video \"${fileName}\" already exists. Skipping this video.\n\n";
    return 0;
  fi

  printf "\n${Title}\n\n"

  printf "Video URL: %s\n\nTitle: %s\nArtists: %s\nThumnail URL: %s\nExtension: %s\nProtocol: %s\n------------------------------------\n" "${VideoURL}" "${Title}" "${Artists}" "${ThumbnailURL}" "${Extension}" "${protocol}"

  printf "\n\n----------------------------------------------\nFull output file path: ${fullFilePath}\n----------------------------------------------\n\n"

  if [[ "${protocol}" ]]; then
    protocolString="[protocol^=${protocol}]"
  else
    protocolString="[protocol^=mp4]"
  fi

  if [[ "$useMetadata" =~ ^TRUE$|^YES$|^[Tt]rue$|^[Yy]es$ ]]; then
#    If using the video's metadata to populate the file name,
#    which I am not currently supporting

#    youtube-dl -v --write-thumbnail --embed-thumbnail --no-overwrites --add-header Referer:"$referrer" -f '(bestvideo+bestaudio/best)'"${protocolString}" -o '\%(title)s - \%(id)s x \%(vcodec)s *** \%(album)s --x-- \%(genre)s %%(release_date).\%(ext)s' --add-header User-Agent:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36" "${VideoURL}"
    dummyVariable54321=''
  else
    # Try Embedding the thumbnail
    # youtube-dl -v --write-thumbnail --embed-thumbnail --no-overwrites --add-header Referer:"$referrer" -f '(bestvideo+bestaudio/best)'"${protocolString}" -o "'${Title}.${Extension}'" --add-header User-Agent:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36" "${VideoURL}"



    youtube-dl -v --write-thumbnail --embed-thumbnail --no-overwrites --add-header Referer:"$referrer" -f '(bestvideo+bestaudio/best)'"${protocolString}" -o "'${Title}.${Extension}'" --add-header User-Agent:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36" "${VideoURL}"
  fi

  if [[ "$trailerURL" ]]; then
    youtube-dl -v --write-thumbnail --embed-thumbnail --no-overwrites --add-header Referer:"$referrer" -f '(bestvideo+bestaudio/best)'"${protocolString}" -o "'${Title}.trailer.${Extension}'" --add-header User-Agent:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36" "${trailerURL}"
  fi

  if [[ "$previewURL" ]]; then
    youtube-dl -v --write-thumbnail --embed-thumbnail --no-overwrites --add-header Referer:"$referrer" -o "'${Title}.preview.${Extension}'" --add-header User-Agent:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36" "${previewURL}"
  fi


  if [ -w "$VideosDirPath" ]; then
    mv "'$fileName'" "$fullFilePath"

    if (( $? > 0 )); then
      mv "$fileName" "$fullFilePath"
    fi
  else
    validatePath "$VideosDirPath"
  fi

  if [[ -d "$TrailersDirPath" ]] && [[ -f "'${Title}.trailer.${Extension}'" ]]; then
    # mv "'${Title}.trailer.${Extension}'" "$TrailersDirPath/${Title}.trailer.${Extension}"
    mv "'$trailerFileName'" "$trailerFullFilePath"

    if (( $? > 0 )); then
      # mv "${Title}.trailer.${Extension}" "$TrailersDirPath/${Title}.trailer.${Extension}"
      mv "$trailerFileName" "$trailerFullFilePath"
    fi
  else
    validatePath "$TrailersDirPath"
  fi

  if [[ -d "$PreviewsDirPath" ]] && [[ -f  "'${Title}.preview.${Extension}'"  ]]; then
    # mv "'${Title}.preview.${Extension}'" "$PreviewsDirPath/${Title}.preview.${Extension}"
    mv "'$previewFileName'" "$previewFullFilePath"

    if (( $? > 0 )); then
      # mv "${Title}.preview.${Extension}" "$PreviewsDirPath/${Title}.preview.${Extension}"
      mv "$previewFileName" "$previewFullFilePath"
    fi
  else
    validatePath "$PreviewsDirPath"
  fi

  if [[ ! -z "${ThumbnailURL}" ]]; then
    printf "Fetching thumbnail....\n\n"
    wget "${ThumbnailURL}" -O "${ThumbnailsDirPath}/${Title}.${ThumbnailExtension}"
  fi

  declare -i randtime=`shuf -i 9-35 -n 1`
  declare -i sleeptime=$(( $randtime < 9 ? 9 : $randtime > 180 ? 180 : $randtime ));
  printf "....Be patient....sleeping "$sleeptime" seconds .....\n";
#  sleep $sleeptime
  sleep 1

  #Debugging
  # printf "\n\n\n\n\n\n";
  # echo "VideosDirPath: " "${VideosDirPath}"
  # echo "ThumbnailsDirPath: " "${ThumbnailsDirPath}"
  # echo "VideosListFileName: " "$VideosListFileName"
  # echo "Protocol string: ${protocol}"
}

if [[ ! -f videos.list ]]; then
  printf "\n\nInstructions:\n\nReplace the example information in \"videos.list\" with the video titles, URLs, and\nother meta-data for the videos you wish to download, then re-run this script.\n\n";
  initVideosList
  exit
fi

if [[ -f .env ]]; then
  source .env
fi

IFS=$'\n'

#  Debugging
# printf "\n\n\n";
# echo "VideosDirPath: " "${VideosDirPath}"
# echo "ThumbnailsDirPath: " "${ThumbnailsDirPath}"
# echo "VideosListFileName: " "$VideosListFileName"
# echo "Protocol string: ${protocol}"
# printf "\n\n\n\n";


if [[ -z "$VideosListFileName" ]]; then
  VideosListFileName=videos.list
fi

if [[ -z "$VideosDirPath" ]]; then
  VideosDirPath=Videos
fi

if [[ -z "$ThumbnailsDirPath" ]]; then
  ThumbnailsDirPath=Thumbnails
fi

if [[ ! -d "$VideosDirPath" ]]; then
  mkdir -p "$VideosDirPath"
  chmod 775 "$VideosDirPath"
fi

if [[ ! -d "$ThumbnailsDirPath" ]]; then
  mkdir -p "$ThumbnailsDirPath"
  chmod 775 "$ThumbnailsDirPath"
fi

if [[ ! -d "$PreviewsDirPath" ]]; then
  mkdir -p "$PreviewsDirPath"
  chmod 775 "$PreviewsDirPath"
fi

while read -r -a line; do
  if [[ -z "$line" ]]; then
    continue;
  fi
  IFS=',' read -r -a data <<< "$line"
  VideoURL="$(trim "${data[0]}")"
  Title="$(trim "${data[1]}")"
  Artists="$(trim "${data[2]}")"
  if [[ "${data[3]}" ]]; then
    ThumbnailURL="$(trim "${data[3]}")"
  else
    ThumbnailURL=''
  fi
#  ThumbnailName="$(trim "${data[3]}")"
  Extension="$(trim "${data[4]}")"

  if [[ ! -z "${data[5]}" ]] && [[ ! "${data[5]}" =~ ^[[:space:]]+$ ]]; then
    protocol="$(trim "${data[5]}")"
  elif [[ -z "${protocol}" ]] && [[ "${Extension}" ]]; then
    protocol="${Extension}"
  elif [[ -z "${protocol}" ]]; then
    protocol=mp4
  fi

  if [[ "${data[6]}" ]]; then
    trailerURL="$(trim "${data[6]}")"
  fi

  if [[ "${data[7]}" ]]; then
    previewURL="$(trim "${data[7]}")"
  fi

  if [[ -z "${Extension}" ]]; then
    Extension=mp4
  fi

  youtubeDl
  # IFS=$'\n'

  unset VideoURL
  unset Title
  unset fileName
  unset fullFilePath
  unset Artists
  unset ThumbnailURL
  unset Extension
  unset trailerURL
  unset trailerFileName
  unset trailerFullFilePath
  unset previewURL
  unset previewFileName
  unset previewFullFilePath

  # In case the protocol varies from file to file
  unset protocol
  if [[ -f .env ]]; then
    source .env
  fi
done < "$VideosListFileName"
