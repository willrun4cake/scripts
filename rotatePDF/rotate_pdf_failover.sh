#!/bin/bash
hasRotate=`type rotate 2>&1`
if [[ ! "$hasRotate" =~ "rotate is aliased to" ]] && [[ ! "$hasRotate" =~ rotate$ ]]; then
  # Create a function as a fail-over to the install script
  function rotate() {
    if [ -f $1 ] || [ -f $2 ]; then
      fileType1=`file "$1"`
      fileType2=`file "$2"`
      if [[ ! "$fileType1" =~ "PDF" ]] && [[ ! "$fileType2" =~ "PDF" ]]; then
        printf "\nError: This file is not a PDF document."
        logger "Error: This file is not a PDF document."
        return -1;
      fi

      has_zenity=`which zenity 2>&1`

      if [ -f "$1" ] && [[ "$fileType1" =~ "PDF" ]]; then
          if [[ ! -z "$1" ]]; then
            filename=`echo "$1" | grep -E "[[:print:]]+" -o`
          fi

          if [[ ! -z "$2" ]]; then
            direction=`echo "$2" | grep -E "[[:print:]]+" -o`
          fi
      elif [ -f "$2" ] && [[ "$fileType2" =~ "PDF" ]]; then
          if [[ ! -z "$1" ]]; then
            direction=`echo "$1" | grep -E "[[:print:]]+" -o`
          fi

          if [[ ! -z "$2" ]]; then
            filename=`echo "$2" | grep -E "[[:print:]]+" -o`
          fi
      else
          printf "\nError: No file name found in argument list.\n"
          logger "\nError: No file name found in argument list.\n"
      fi

      has_pdftk=`which pdftk 2>&1`
      range="1-end"
      dateStr=`date +"%s"`
      tempStr="tempFilePDF""$dateStr"".pdf"

      if [[ -z "$has_pdftk" ]] || [[ ! "$has_pdftk" =~ "pdftk" ]]; then
        if [[ ! -z "$has_zenity" ]] && [[ "$has_zenity" =~ "zenity" ]]; then
            zenity --question --title="Install PDFtk" --text="PDFtk must be installed to use this command\nWould you like to install it using apt?" --timeout=6

            if (( $? == 0 ))
            then
              sudo apt-get install pdftk
              printf "\n\n\n";
            else
              printf "\nYou decided not to install PDFtk. Exiting.....\n";
              return -1;
            fi
        else
          read -p "PDFtk must be installed to run this command.\nWould you like to install it using apt?\n" -r

          if [[ ! "$REPLY" =~ ^[Yy] ]]; then
            printf "\nYou decided not to install PDFtk. Exiting.....\n";
            return -1;
          else
            sudo apt-get install pdftk
            printf "\n\n\n";
          fi
        fi
      fi

      shopt -s nocasematch

      if [[ ! "$direction" =~ "not found" ]]; then
        if [[ ! -z "$direction" ]]; then
          if [[ "$direction" == "right" ]] || [[ "$direction" == "90" ]] || [[ "$direction" == "east" ]]; then
            direction="right"
          elif [[ "$direction" == "left" ]] || [[ "$direction" == "-90" ]] || [[ "$direction" == "west" ]] || [[ "$direction" == "270" ]]; then
            direction="left"
          elif [[ "$direction" == "down" ]] || [[ "$direction" == "180" ]] || [[ "$direction" == "-180" ]] || [[ "$direction" == "south" ]]; then
            direction="down"
          elif [[ "$direction" == "up" ]] || [[ "$direction" == "360" ]]; then
            direction="north"
          elif [[ "$direction" == "360" ]] || [[ "$direction" == "north" ]]; then
            direction="north"
          else
            printf "\nError: $direction is not a valid rotation direction.\n"
            logger "Error: $direction is not a valid rotation direction.\n"
            return -1;
          fi

          printf "\nRotating document: $filename"
          logger "Rotating document: $filename"

          pdftk "$filename" cat "$range""$direction" output "$tempStr" 2>&1 && mv "$tempStr" "$filename" 2>&1 && rm -f "$tempStr" 2>&1
          printf "\nFinished."
        else
          printf "\nError: you must provide the rotation direction as an argument to rotate().\n";
          logger "Error: you must provide the rotation direction as an argument to rotate().";
          shopt -u nocasematch
          return -1;
        fi
      fi

      shopt -u nocasematch
    else
      printf "\nError: $filename is not a file.\n"
      logger "Error: $filename is not a file."
      return -1;
    fi
  }
fi

if [ ! -f "$HOME"/.local/share/nautilus/scripts/Flip\ Right ]; then
  cat "$HOME"/.local/share/nautilus/scripts/Flip\ Right <<- 'EOF'
    #!/bin/bash
    source /home/joe/scripts/rotatePdf.sh

    IFS=$'\n' read -rd '' -a filenames <<<"$NAUTILUS_SCRIPT_SELECTED_FILE_PATHS"

    hasRotate=`type rotate`
    if [[ "$hasRotate" =~ "rotate is" ]];then
      for i in "${filenames[@]}"
      do
        file=`echo "$i" | grep -E "[[:print:]]+" -o`
        rotate "$file" "right"
      done
    fi
  EOF
fi

if [ ! -f "$HOME"/.local/share/nautilus/scripts/Flip\ Left ];then
  cat "$HOME"/.local/share/nautilus/scripts/Flip\ Left <<- 'EOF'
  #!/bin/bash
  source /home/joe/scripts/rotatePdf.sh

  IFS=$'\n' read -rd '' -a filenames <<<"$NAUTILUS_SCRIPT_SELECTED_FILE_PATHS"

  hasRotate=`type rotate`
  if [[ "$hasRotate" =~ "rotate is" ]];then
    for i in "${filenames[@]}"
    do
      file=`echo "$i" | grep -E "[[:print:]]+" -o`
      rotate "$file" "left"
    done
  fi
  EOF
fi

# Post install script
# alias rotate=/usr/bin/rotatepdf
