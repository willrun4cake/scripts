#!/bin/sh
#
#   Since the DEB package should not write to any user custom files (.bashrc)
#   create an optional install script if the user wants to add the
#   GUI-based context menu items.
#
#

printf "\n\n\nStarting the GUI customization script now......\n\n";

# Xfce Thunar GUI
tcxml="$HOME"/.config/Thunar/uca.xml

if [ -f "$tcxml" ]; then
  sed -i "/<\/actions>/d" "$tcxml"
  cat <<- EOF >> "$tcxml"
  <action>
  	<icon>gimp</icon>
  	<name>Flip Right</name>
  	<unique-id>1670080103801-1</unique-id>
  	<command>rotatepdf right %f</command>
  	<description>Rotate a PDF file 90"\xe2\x84\x83" clockwise (all pages).</description>
  	<patterns>*</patterns>
  	<startup-notify/>
  	<pdf-files/>
  </action>
EOF
  echo "</actions>" >> "$tcxml"
fi

exit 0
