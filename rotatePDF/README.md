Below is an article on how to publish a Debian package:
  https://arnaudr.io/2016/10/01/publishing-a-debian-package-mentors-sponsorship/

Below are example commands instructing how use dh_make to build a new package:

mkdir my-new-package-1.0.0
cd my-new-package-1.0.0
cp ../packageBinary .
cp ../packageScript .
sudo apt-get install build-essential autoconf automake autotools-dev dh-make debhelper devscripts fakeroot xutils lintian pbuilder
dh_make --createorig
nano debian/control
nano debian/copyright
nano debian/changelog
# mv debian/postinst.ex debian/postinst
# mv debian/postrm.ex debian/postrm
dpkg-buildpackage -rfakeroot -uc -us
lintian ../my-new-package_1.0.0_all.deb
