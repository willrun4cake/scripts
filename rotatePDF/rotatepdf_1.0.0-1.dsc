Format: 3.0 (quilt)
Source: rotatepdf
Binary: rotatepdf
Architecture: all
Version: 1.0.0-1
Maintainer: Joseph Warner <devjw29@gmail.com>
Homepage: https://www.bing.com/
Standards-Version: 4.4.1
Build-Depends: pdftk, debhelper-compat (= 12)
Package-List:
 rotatepdf deb utils optional arch=all
Checksums-Sha1:
 8f5c4b1b26afd688d5ee65c4fa6e18e85355deb8 8116 rotatepdf_1.0.0.orig.tar.xz
 387a3d7f78b92403150c096882ea81161fcbedcf 5628 rotatepdf_1.0.0-1.debian.tar.xz
Checksums-Sha256:
 c7727b51109593dfa6e6b2afe5b428a7710882a7f940aefe31ec276b7c00b44a 8116 rotatepdf_1.0.0.orig.tar.xz
 2e513c7c2524782e8ebb72526806a4f4478974db61e3481187304a9fd53523a5 5628 rotatepdf_1.0.0-1.debian.tar.xz
Files:
 27942818cfb0387bad419797dcfea1b5 8116 rotatepdf_1.0.0.orig.tar.xz
 836533330ddba5a935138f8d20a6b70a 5628 rotatepdf_1.0.0-1.debian.tar.xz
